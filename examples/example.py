import sys, os
sys.path.insert(0, os.path.abspath(__file__+'../../..'))

import guidebox, json
guidebox.api_key = "YOUR KEY GOES HERE" # Replace this API key with your own.

# Fetch movies
movies = guidebox.Movie.list()

# print movie titles
for movie in movies['results']:
    print(movie.title)
